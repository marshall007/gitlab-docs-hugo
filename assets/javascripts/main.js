import 'bootstrap';

import './docs';
import './global-nav';
import './badges';
import './banner';
import './toggle_popover';