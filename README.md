# GitLab Documentation (Hugo)

Currently this project expects the source doc repos to be cloned in the
same directory as this project.

You must also rename `index.md` files to `_index.md` in the source repos,
for example:

```sh
zmv '../gitlab/doc/(**)/index.md' '../gitlab/doc/$1/_index.md'
zmv '../gitlab-runner/docs/(**)/index.md' '../gitlab-runner/docs/$1/_index.md'
```

Then start Hugo:

```sh
hugo serve
```